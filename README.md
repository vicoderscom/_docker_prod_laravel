# Vicoders Laravel Docker

- [Vicoders Laravel Docker](#vicoders-laravel-docker)
  - [MySQL](#mysql)
  - [Supervisor](#supervisor)
  - [Cronjob](#cronjob)

## MySQL

```
mkdir -p var/lib/mysql
chown -R 1001:1001 var/lib
```

```
mkdir -p etc/bitnami/mysql/conf
chown -R 1001:1001 etc/bitnami
```

## Supervisor

Tất cả các file supervisor config có trong folder `/workspace/.supervisord/*.ini` sẽ được load

## Cronjob

update `./docker/server/Dockerfile`
